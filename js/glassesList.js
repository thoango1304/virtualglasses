export class GlassesList {
    constructor() {
        this.gList = [];
    }

    addGlasses(glasses) {
        this.gList.push(glasses);
    }

    renderGlassesList() {
        let content = "";

        content = this.gList.reduce((gContent, item) => {
            gContent += `
                <div class="col-4">
                    <img data-id="${item.id}" src="${item.src}" class="img-fluid" onclick="tryOnGlasses(event)">
                </div>
            `;

            return gContent;
        }, "")

        return content;
    }
}