let dataGlasses = [
    { id: "G1", src: "./img/g1.jpg", virtualImg: "./img/v1.png", brand: "Armani Exchange", name: "Bamboo wood", color: "Brown", price: 150, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis ea voluptates officiis? " },
    { id: "G2", src: "./img/g2.jpg", virtualImg: "./img/v2.png", brand: "Arnette", name: "American flag", color: "American flag", price: 150, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. In assumenda earum eaque doloremque, tempore distinctio." },
    { id: "G3", src: "./img/g3.jpg", virtualImg: "./img/v3.png", brand: "Burberry", name: "Belt of Hippolyte", color: "Blue", price: 100, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit." },
    { id: "G4", src: "./img/g4.jpg", virtualImg: "./img/v4.png", brand: "Coarch", name: "Cretan Bull", color: "Red", price: 100, description: "In assumenda earum eaque doloremque, tempore distinctio." },
    { id: "G5", src: "./img/g5.jpg", virtualImg: "./img/v5.png", brand: "D&G", name: "JOYRIDE", color: "Gold", price: 180, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Error odio minima sit labore optio officia?" },
    { id: "G6", src: "./img/g6.jpg", virtualImg: "./img/v6.png", brand: "Polo", name: "NATTY ICE", color: "Blue, White", price: 120, description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit." },
    { id: "G7", src: "./img/g7.jpg", virtualImg: "./img/v7.png", brand: "Ralph", name: "TORTOISE", color: "Black, Yellow", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim sint nobis incidunt non voluptate quibusdam." },
    { id: "G8", src: "./img/g8.jpg", virtualImg: "./img/v8.png", brand: "Polo", name: "NATTY ICE", color: "Red, Black", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, unde enim." },
    { id: "G9", src: "./img/g9.jpg", virtualImg: "./img/v9.png", brand: "Coarch", name: "MIDNIGHT VIXEN REMIX", color: "Blue, Black", price: 120, description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit consequatur soluta ad aut laborum amet." }
];

import { Glasses } from "./glasses.js";
import { GlassesList } from "./glassesList.js";

let glassesList = new GlassesList();

const getELE = (id) => {
    return document.getElementById(id);
}

const showGlassesList = () => {
    let divGlassesList = getELE("vglassesList");

    dataGlasses.map((item) => {
        let glasses = new Glasses(item.id, item.src, item.virtualImg, item.brand, item.name, item.color, item.price, item.description);
        glassesList.addGlasses(glasses);
    })

    divGlassesList.innerHTML = glassesList.renderGlassesList();
}

showGlassesList();
 
const tryOnGlasses = (e) => {
    let idObject = e.target.getAttribute("data-id");
    let objectGlasses = {};

    glassesList.gList.map((item) => {
        if (item.id == idObject) {
            objectGlasses = item;
        }
    })

    showInfo(objectGlasses);
}

window.tryOnGlasses = tryOnGlasses;

const showInfo = (objGlasses) => {
    let divAvatar = getELE("avatar");
    let divInfo = getELE("glassesInfo");

    if (objGlasses == null) {
        return;
    }

    divAvatar.innerHTML = `
        <img id="glasses" src="${objGlasses.virtualImg}">
    `;

    let status = "";

    if (objGlasses.status) {
        status = "Stocking";
    } else {
        status = "Sold Out";
    }

    divInfo.innerHTML = `
        <h5>${objGlasses.name} - ${objGlasses.brand} (${objGlasses.color})</h5>
        <p class="card-text">
            <span class="btn btn-success btn-sm mr-2">${objGlasses.price}</span>
            <span>${status}</span>
        </p>
        <p class=""card-text>${objGlasses.desc}</p>
    `;

    divInfo.style.display = "block";
}

const removeGlasses = (isDisplay) => {
    let glasses = getELE("glasses");

    if (glasses == null) {
        return;
    }

    if (isDisplay) {
        glasses.style.display = "block";
    } else {
        glasses.style.display = "none";
    }
}

window.removeGlasses = removeGlasses;